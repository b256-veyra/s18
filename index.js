// console.log("Hello World")

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	//first function here:
	function profile() {
		let name = prompt("What is your Name");
		let age = prompt("How old are you");
		let location = prompt("Where do you live");
		alert("Thank you for your input!");

		console.log("Hello, " + name );
		console.log("You are " + age + " years old");
		console.log("You live in " + location);
	}

	profile();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function topFiveMusicArtists() {
	console.log("1. Post Malone");
	console.log("2. The Beatles");
	console.log("3. Micheal Bubble");
	console.log("4. Lionel Richie");
	console.log("5. Justine Bieber");
};

topFiveMusicArtists();

	

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function top5FavoriteMovies() {
	console.log("1. Avengers: End Game");
	console.log("Rotten Tomatoes Rating: 80%");
	console.log("2. The Wolf of Wallstreet");
	console.log("Rotten Tomatoes Rating: 83%");
	console.log("3. Instertellar");
	console.log("Rotten Tomatoes Rating: 94%");
	console.log("4. The Matrix");
	console.log("Rotten Tomatoes Rating: 86%");
	console.log("5. Lord of the Rings");
	console.log("Rotten Tomatoes Rating: 97%");
}

top5FavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function() {
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2); 
